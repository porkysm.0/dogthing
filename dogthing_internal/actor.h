#pragma once

#include "character.h"
#include "memory.h"
#include "vector3.h"
#include "types.h"

// The primary non-geometry objects within a level.
// Power-ups are not considered actors.
class Actor {
public:
    OFFSET_POINTER_FUNC(this, Character*, character, 0xF0)
    OFFSET_POINTER_FUNC(this, Vector3, position, 0x3C)
    OFFSET_POINTER_FUNC(this, ActorType, actor_type, 0x64)
    OFFSET_POINTER_FUNC(this, int, animation_speed, 0xB4)
    OFFSET_POINTER_FUNC(this, Vector3, velocity, 0xD0)
};
