#pragma once

// Used to define inline class methods for retrieving pointers to variables at a given offset
#define OFFSET_POINTER_FUNC(object, type, name, offset) \
    inline type* name##_ptr() { return (type*)((unsigned int)object + (unsigned int)offset); }

// Memory utils
namespace memory {

}