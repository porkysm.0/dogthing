#include <windows.h>

#include "dogthing.h"

HINSTANCE g_mainModule;

/**
 * Dll Entrypoint
 */
extern "C" BOOL WINAPI DllMain(HINSTANCE module, DWORD reason, LPVOID reserved)
{
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        g_mainModule = module;

        CreateThread(0, 0, (LPTHREAD_START_ROUTINE)dogthing::thread_init, 0, 0, 0);

        break;
    }

    return TRUE;
}
