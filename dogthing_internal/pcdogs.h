#pragma once

#include <windows.h>

#include "actor.h"
#include "linkedlist.h"

// The primary class for interfacing internally with 102 Dalmatians.
class PCDogs {
public:
    // Initializes an instance of PCDogs
    PCDogs(HMODULE module);

    // The container class for a linked list of actors.
    LinkedList<Actor>* actor_list;
};
