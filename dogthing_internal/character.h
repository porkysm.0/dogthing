#pragma once

#include "memory.h"

// A subclass of Actor that stores data regarding health, movement speed, controller, etc.
// Only present for the player and AI.
class Character {
public:
    OFFSET_POINTER_FUNC(this, int, id, 0x16C)
    OFFSET_POINTER_FUNC(this, int, health, 0x74)
    OFFSET_POINTER_FUNC(this, int, speed, 0x48)
};
