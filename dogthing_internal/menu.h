#pragma once

class PCDogs;

namespace menu {
// Renders the menu in the global ImGui context
void render_menu(PCDogs* game);
}