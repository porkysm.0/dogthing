#pragma once

#include <vector>

// Used internally to form linked lists (primarily in actor and entity sets).
template <class T>
class ListNode {
public:
    ListNode* next;
    T element;
};

// Wraps a ListNode list and provides methods for modifying the list.
template <class T>
class LinkedList {
public:
    ListNode<T>* first_node;

    LinkedList()
    {
    }

    LinkedList(ListNode<T>* first_node)
    {
        this->first_node = first_node;
    }

    std::vector<T*> get_vector()
    {
        std::vector<T*> elements;
        ListNode<T>* element = first_node;
    
		while (element != nullptr) {
            elements.push_back(&element->element);

            element = element->next;
		}

		return elements;
	}

    bool contains(T* element)
    {
        ListNode<T>* current_node = first_node;

        while (current_node != nullptr) {
            if (&current_node->element == element)
                return true;

            current_node = current_node->next;
        }

        return false;
    }

    int length()
    {
        int len = 0;
        ListNode<T>* current_node = first_node;

        while (current_node != nullptr) {
            len++;
            current_node = current_node->next;
        }

        return len;
    }
};
