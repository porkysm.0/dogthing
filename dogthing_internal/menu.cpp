#include <stdio.h>

#include <algorithm>
#include <vector>

#include "menu.h"

#include "pcdogs.h"

#include "imgui.h"

static Actor* selected_actor;

bool sort_actors(Actor* a, Actor* b) { return (unsigned int)a < (unsigned int)b; }

void menu::render_menu(PCDogs* game)
{
    ImGui::Begin("Actor List");
    ImGui::ListBoxHeader("");

    std::vector<Actor*> actors = game->actor_list->get_vector();
    bool actor_selected;

    std::sort(actors.begin(), actors.end(), sort_actors);

    for (Actor* actor : actors) {
        static char actor_selectable_title[0x100];

        if (actor_type_names.count(*actor->actor_type_ptr()) > 0)
            sprintf_s(actor_selectable_title, "%s | 0x%X", actor_type_names[*actor->actor_type_ptr()], (unsigned int)actor);
        else
            sprintf_s(actor_selectable_title, "Unknown (Type %d) | 0x%X", (int)*actor->actor_type_ptr(), (unsigned int)actor);

        if (!ImGui::Selectable(actor_selectable_title, &actor_selected))
            continue;

        selected_actor = actor;
    }

    ImGui::ListBoxFooter();

    if (selected_actor != nullptr && game->actor_list->contains(selected_actor)) {
        ImGui::LabelText("Address", "%X", &selected_actor);

        ImGui::InputInt3("Position", (int*)selected_actor->position_ptr());
        ImGui::InputInt3("Velocity", (int*)selected_actor->velocity_ptr());

        Character* c = *selected_actor->character_ptr();

        if (c != nullptr) {
            ImGui::InputInt("Type", (int*)selected_actor->actor_type_ptr());
            ImGui::InputInt("ID", c->id_ptr());
        }
    }

    ImGui::End();
}