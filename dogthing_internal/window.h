#pragma once

class PCDogs;

namespace window {
// The main thread and initialization function for the window
// Should be given its own thread
// Maybe I should change this to be called during a game routine?
void window_main(PCDogs* game);
}