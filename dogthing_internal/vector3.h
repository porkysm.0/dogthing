#pragma once

// Used internally by the game to store 3D vector data
class Vector3 {
public:
    Vector3();
    Vector3(int x, int y, int z);

    int x, y, z;
};
