#include "dogthing.h"
#include "window.h"

#include "pcdogs.h"

void dogthing::thread_init()
{
    PCDogs* game = new PCDogs(GetModuleHandleA("pcdogs.exe"));
    window::window_main(game);
}
