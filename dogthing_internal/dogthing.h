#pragma once

namespace dogthing {
// Serves as the main initialization thread called on injection
void thread_init();
}
