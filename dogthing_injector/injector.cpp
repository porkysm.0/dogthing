#include <windows.h>

#include <psapi.h>
#include <tlhelp32.h>

#include "injector.h"

void injector::enable_debug_privileges()
{
    HANDLE token;
    TOKEN_PRIVILEGES new_priv, old_priv;
    LUID luid;
    DWORD return_length;

    OpenProcessToken(NULL, 40, &token);
    LookupPrivilegeValue(NULL, "SeDebugPrivilege", &luid);
    new_priv.PrivilegeCount = 1;
    new_priv.Privileges[0].Luid = luid;
    new_priv.Privileges[0].Attributes = 2;
    AdjustTokenPrivileges(token, 0, &new_priv, 28, &old_priv, &return_length);
}

HANDLE injector::get_process(const char* process_name)
{
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    if (Process32First(snapshot, &entry)) {
        while (Process32Next(snapshot, &entry)) {
            if (_stricmp(process_name, entry.szExeFile) == FALSE) {
                CloseHandle(snapshot);
                return OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
            }
        }
    }

    CloseHandle(snapshot);
    return NULL;
}

HANDLE injector::inject_library(HANDLE process, const char* library_path)
{
    HMODULE kernel_handle = GetModuleHandleA("kernel32.dll");

    if (!kernel_handle)
        return NULL;

    DWORD loadlibrary = (DWORD)GetProcAddress(kernel_handle, "LoadLibraryA");
    void* remote_string = VirtualAllocEx(process, NULL, strlen(library_path), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

    if (!remote_string)
        return NULL;

    WriteProcessMemory(process, remote_string, library_path, strlen(library_path), NULL);

    return CreateRemoteThread(process, NULL, 0, (LPTHREAD_START_ROUTINE)loadlibrary, remote_string, 0, NULL);
}

int injector::get_executable_directory(char* dir)
{
    char module_name[MAX_PATH];

    GetModuleBaseNameA(GetCurrentProcess(), NULL, module_name, MAX_PATH);
    GetModuleFileName(NULL, dir, MAX_PATH);

    memset(dir + strlen(dir) - strlen(module_name), 0, strlen(module_name));

    return strlen(dir);
}
