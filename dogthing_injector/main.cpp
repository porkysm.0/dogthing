#include <iostream>
#include <windows.h>

#include "injector.h"

int main()
{
    SetConsoleTitle("DogThing2");

    printf("Enabling debug privileges...");
    injector::enable_debug_privileges();
    printf("OK\n");

    printf("Searching for game process...");
    HANDLE process = injector::get_process("pcdogs.exe");

    if (!process) {
        printf("FAILED\nProcess not found.");
        return 1;
    }
    printf("OK\n");

    printf("Injecting library...");

    char exe_directory[MAX_PATH];

    unsigned int exe_length = injector::get_executable_directory(exe_directory);
    strcat(exe_directory, "\\dogthing_internal.dll");
    injector::inject_library(process, exe_directory);
    CloseHandle(process);
    printf("OK\n");

    return 0;
}
