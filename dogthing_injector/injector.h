#pragma once

#include <Windows.h>

namespace injector {
	// Enables debug privileges for writing arbitrary process memory
	void enable_debug_privileges();

	// Gets a process handle by its process executable name
	HANDLE get_process(const char* process_name);

	// Injects a library into the given target process
	HANDLE inject_library(HANDLE process, const char* library_path);

	// Gets the absolute directory of the current executable
	int get_executable_directory(char* dir);
}
